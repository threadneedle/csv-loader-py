"""
Example Python 2.7 script to read a CSV file of a Solidatus model export and re-import it into Solidatus as a model
using the API
"""

import logging
import csv
import requests
import json as JSON
import argparse
import urllib
import codecs

from uuid import uuid4

_ID = 'ID'
_NAME = 'NAME'
_TYPE = 'TYPE'
_PARENT = 'PARENT'
_LAYER = 'Layer'
_OBJECT = 'Object'
_GROUP = 'Group'
_ATTRIBUTE = 'Attribute'
_TRANSITION = 'Transition'


# Class to group CSV rows by Type
class DataRows:
    def __init__(self):
        self.layers = [ ]
        self.objects = [ ]
        self.groups = [ ]
        self.attributes = [ ]
        self.transitions = [ ]


_DATA_ROWS = DataRows()


# CLass containing the entities to represent the Solidatus Model
class SolidatusModel:
    def __init__(self):
        self.entities = {}
        self.transitions = {}
        self.roots = []


model = SolidatusModel()


# Configure Logging options
logging.basicConfig(level=logging.INFO)

# Parse the Command Line arguments
parser = argparse.ArgumentParser()
parser.add_argument("--input", help="CSV input file", required=True)
parser.add_argument('--host',  help="Solidatus host url e.g. https://demo.solidatus.com", required=True)
parser.add_argument('--token', help="Solidatus API access token", required=True)
parser.add_argument('--model', help="Name of new or existing model", required=True)
args = parser.parse_args()

# Read the header from the CSV file and create entities for them
with codecs.open(args.input, encoding='utf-8-sig') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    line_count = 0
    for row in csv_reader:
        if line_count == 0:
            logging.debug("Headers are  {}".format(row))
            _HEADER_ROW = row
        else:
            logging.debug("Row: {} -> {}".format(line_count, row))
            dataRow = {}
            for field in row:
                dataRow[_HEADER_ROW[row.index(field)]] = field

            if dataRow[_TYPE] == _LAYER:
                _DATA_ROWS.layers.append(dataRow)
            elif dataRow[_TYPE] == _GROUP:
                _DATA_ROWS.groups.append(dataRow)
            elif dataRow[_TYPE] == _OBJECT:
                _DATA_ROWS.objects.append(dataRow)
            elif dataRow[_TYPE] == _ATTRIBUTE:
                _DATA_ROWS.attributes.append(dataRow)
            elif dataRow[_TYPE] == _TRANSITION:
                _DATA_ROWS.transitions.append(dataRow)

        line_count += 1

# Create an index of the Properties values
PropertiesIndex = _HEADER_ROW[_HEADER_ROW.index('PROPERTIES:') + 1:_HEADER_ROW.__len__()]

# Add Layers to Model.roots and Model.entities
for layer in _DATA_ROWS.layers:
    if not layer[_ID]:
        logging.error('Layer does not have an ID name={}'.format(layer[_NAME]))
        exit(1)
    model.roots.append(layer[_ID])
    model.entities[layer[_ID]] = {'name': layer[_NAME], 'properties': {}, 'children': []}
    for prop in PropertiesIndex:
        if prop in layer:
            model.entities[layer[_ID]]['properties'][prop] = layer[prop]

# Add Objects to Model.entities and as children of Layers
for obj in _DATA_ROWS.objects:
    if not obj[_ID]:
        logging.error('Object does not have an ID name={}'.format(obj[_NAME]))
        exit(1)
    model.entities[obj[_ID]] = {'name': obj[_NAME], 'properties': {}, 'children': []}
    if obj[_PARENT] in model.entities:
        model.entities[obj[_PARENT]]['children'].append(obj[_ID])
    else:
        logging.warning('Failed to find parent for Object id={} entity will be removed from model !'.format(obj[_ID]))
        del model.entities[obj[_ID]]

    for prop in PropertiesIndex:
        if prop in obj:
            model.entities[obj[_ID]]['properties'][prop] = obj[prop]

# Add Groups to Model.Entities
for group in _DATA_ROWS.groups:
    if not group[_ID]:
        logging.error('Group does not have an ID name={}'.format(group[_NAME]))
        exit(1)
    model.entities[group[_ID]] = {'name': group[_NAME], 'properties': {}, 'children': []}
    for prop in PropertiesIndex:
        if prop in group:
            model.entities[group[_ID]]['properties'][prop] = group[prop]
# Add Groups as Children of Objects or other Groups
for group in _DATA_ROWS.groups:
    if group[_PARENT] in model.entities:
        model.entities[group[_PARENT]]['children'].append(group[_ID])
    else:
        logging.warning('Failed to find parent for Group id={} entity will be removed from model !'.format(group[_ID]))
        del model.entities[group[_ID]]

# Add Attributes to Model.entities
for attribute in _DATA_ROWS.attributes:
    if not attribute[_ID]:
        logging.error('Attribute does not have an ID name={}'.format(attribute[_NAME]))
        exit(1)
    model.entities[attribute[_ID]] = {'name': attribute[_NAME], 'properties': {}, 'children': []}
    for prop in PropertiesIndex:
        if prop in attribute:
            model.entities[attribute[_ID]]['properties'][prop] = attribute[prop]
# Add Attributes as Children of entities in Model.entities
for attribute in _DATA_ROWS.attributes:
    if attribute[_PARENT] in model.entities:
        model.entities[attribute[_PARENT]]['children'].append(attribute[_ID])
    else:
        logging.warning('Failed to find parent for Attribute id={} entity will be removed from model !'.format(attribute[_ID]))
        del model.entities[attribute[_ID]]

# Add Transitions to Model.Transitions
for transition in _DATA_ROWS.transitions:
    if transition['SOURCE'] in model.entities and transition['TARGET'] in model.entities:
        transition[_ID] = uuid4().__str__()
        model.transitions[transition[_ID]] = {'source': transition['SOURCE'], 'target': transition['TARGET'], 'properties': {}}
        for prop in PropertiesIndex:
            if prop in transition:
                model.transitions[transition[_ID]]['properties'][prop] = transition[prop]
    else:
        logging.warning('Transition with id={} source={} target={} has a missing source or target entity !'.format(transition[_ID], transition['SOURCE'], transition['TARGET']))

logging.debug(JSON.dumps(model.__dict__, indent=1))

# Create dictionary object representing a ReplaceModel command using the model above
command = {
    'model': model.__dict__,
    'cmd': 'ReplaceModel',
    'comparator': {
        'path': 'true'
    }
}

# Build a request for Command above
request = {
    'cmds': [command],
    'commit': 'true'
}

# Create the headers for our API requests
headers = {
    'Content-Type': 'application/json',
    'Authorization': 'Bearer ' + args.token
}

logging.debug('Request=' + JSON.dumps(request, indent=1))

# Search for the model to update
jsonResponse = requests.get(args.host + '/api/v1/models?searchTerm={}'.format(urllib.quote(args.model)),
                            headers=headers)

# Check for an error with the search
if jsonResponse.status_code != 200:
    logging.fatal('ERROR: ' + jsonResponse.content + ' ' + jsonResponse.reason)
    exit(1)
else:
    logging.debug('Response=' + jsonResponse.text)

# Convert the JSON response to a Dictionary object
objResponse = JSON.loads(jsonResponse.content)

# Model Id variable
modelId = {}

# Check the items returned in the Search response
if objResponse['total'] > 1:
    logging.fatal('Found more than one model match searchTerm={}'.format(args.model))
    exit(1)
elif objResponse['total'] == 0:
    logging.info('Model does not exist, creating model {}'.format(args.model))
    data = {
        'name': args.model
    }
    createModelResponse = requests.post(args.host + '/api/v1/models', data=JSON.dumps(data), headers=headers)
    if createModelResponse.status_code != 200:
        logging.fatal('ERROR: Failed to create model !')
        exit(1)
    elif createModelResponse.status_code == 200:
        json = JSON.loads(createModelResponse.content)
        modelId = json['id']
elif objResponse['total'] == 1:
    modelId = objResponse['items'][0]['id']

# Execute the update Command request against Solidatus API
updateModelResponse = requests.post(args.host + '/api/v1/models/{}/update'.format(modelId), data=JSON.dumps(request),headers=headers)

# Check for an error with the ReplaceModel command
if updateModelResponse.status_code != 200:
    logging.fatal('ERROR: Failed to update model modelId={}'.format(modelId))
    logging.error(updateModelResponse.reason)
    exit(1)
elif updateModelResponse.status_code == 200:
    logging.info('Model {} has been updated.'.format(args.model))
