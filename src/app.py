import logging
import csv
import requests
import uuid
import json as JSON
import argparse
import urllib

"""
Example Python 2.7 script to read a CSV file and create a Solidatus model of the file headers
"""

# Configure Logging options
logging.basicConfig(level=logging.INFO)

# Parse the Command Line arguments
parser = argparse.ArgumentParser()
parser.add_argument("--input", help="CSV input file", required=True)
parser.add_argument('--host', help="Solidatus host url e.g. https://demo.solidatus.com", required=True)
parser.add_argument('--token', help="Solidatus API access token", required=True)
parser.add_argument('--model', help="Name of new or existing model", required=True)
args = parser.parse_args()

# Solidatus Entities model
entities = {
    'layer': {
        'children': ['object'],
        'name': 'Files'
    },
    'object': {
        'children': [],
        'name': args.input
    }
}

# Read the header from the CSV file and create entities for them
with open(args.input) as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    line_count = 0
    for row in csv_reader:
        if line_count == 0:
            logging.info("Headers are  {}".format(row))
            for header in row:
                id = uuid.uuid4().__str__()
                entities[id] = {'name': header}
                entities['object']['children'].append(id)
            break
logging.debug('Processed {} lines.'.format(line_count + 1))

# Create dictionary object containing the entities to represent the Solidatus model
model = {
    'entities': entities,
    'transitions': {},
    'roots': ['layer']
}

# Create dictionary object representing a ReplaceModel command using the model above
command = {
    'model': model,
    'cmd': 'ReplaceModel',
    'comparator': {
        'name': 'true'
    }
}

# Buidl a request for Command above
request = {
    'cmds': [command],
    'commit': 'true'
}

# Create the headers for our API requests
headers = {
    'Content-Type': 'application/json',
    'Authorization': 'Bearer ' + args.token
}

logging.debug('Request=' + JSON.dumps(request, indent=1))

# Search for the model to update
jsonResponse = requests.get(args.host + '/api/v1/models?searchTerm={}'.format(urllib.parse.quote(args.model)),
                            headers=headers)

# Check for an error with the search
if jsonResponse.status_code != 200:
    logging.fatal('ERROR: ' + jsonResponse.content + ' ' + jsonResponse.reason)
    exit(1)
else:
    logging.debug('Response=' + jsonResponse.text)

# Convert the JSON response to a Dictionary object
objResponse = JSON.loads(jsonResponse.content)

# Model Id variable
modelId = {}

# Check the items returned in the Search response
if objResponse['total'] > 1:
    logging.fatal('Found more than one model match searchTerm={}'.format(args.model))
    exit(1)
elif objResponse['total'] == 0:
    logging.info('Model does not exist, creating model {}'.format(args.model))
    data = {
        'name': args.model
    }
    createModelResponse = requests.post(args.host + '/api/v1/models', data=JSON.dumps(data), headers=headers)
    if createModelResponse.status_code != 200:
        logging.fatal('ERROR: Failed to create model !')
        exit(1)
    elif createModelResponse.status_code == 200:
        json = JSON.loads(createModelResponse.content)
        modelId = json['id']
elif objResponse['total'] == 1:
    modelId = objResponse['items'][0]['id']

# Execute the update Command request against Solidatus API
updateModelResponse = requests.post(args.host + '/api/v1/models/{}/update'.format(modelId), data=JSON.dumps(request),
                                    headers=headers)

# Check for an error with the ReplaceModel command
if updateModelResponse.status_code != 200:
    logging.fatal('ERROR: Failed to update model modelId={}'.format(modelId))
    logging.error(updateModelResponse.reason + ' ' + updateModelResponse.content)
    exit(1)
elif updateModelResponse.status_code == 200:
    logging.info('Model {} has been updated.'.format(args.model))
