# csv-loader-py

Requires python 3

To install dependencies run
```
pip install -r requirements.txt
```

## CSV Utility

This utility will read the header row, expected to be the first row, of a comma delimited file, and create a Solidatus model
to represent the file structure.


To run the CSV utility:
```
python src/app.py --in=abcdef.csv --host=http://localhost --token=SOLIDATUS_TOKEN --model="Python CSV loader"
```

Command line options

|Option|Description|
|-------|----------|
|-h, --help|Help information|
|--input=INPUT|CSV import file|
|--host=$HOST|Solidatus host e.g. https://demo.solidatus.com|
|--token=$TOKEN|Solidatus API access token|
|--model=$MODEL|Name of new or existing model|
|--verifySSL=$FLAG|Verify SSL certificate, True or False, default is False|

## Tabular Model Import Utility

This utility will read a Tabular Export file of a model from Solidatus, and re-import it back into Solidatus.
The expected columns should match the default columns exported by Solidatus.

|Default Export Column | Description |
|----------------------|-------------|
|ID| id of the entity or transition |
|TYPE|Layer , Object , Attribute , Transition |
|PARENT| id of the parent entity or blank |
|NAME| name of the object
|SOURCE| id of the source entity of a transition |
|TARGET| if of the target entity of a transition |
|blank| the column is deliberately left blank |
|PROPERTIES:| The properties section marker |
|property 0:N| All columns now represent entity properties and maybe sparsely populated |


To run the Import utility:
```
python src/import.py --in=tabular-model.csv --host=http://localhost --token=SOLIDATUS_TOKEN --model="Python CSV loader"
```

Command line options

|Option|Description|
|-------|----------|
|-h, --help|Help information|
|--input=$INPUT|Tabular model definition file|
|--host=$HOST|Solidatus host e.g. https://demo.solidatus.com|
|--token=$TOKEN|Solidatus API access token|
|--model=$MODEL|Name of new or existing model|
|--verifySSL=$FLAG|Verify SSL certificate, True or False, default is False|
